#!/usr/bin/python           # This is client.py file
 
from socket import * 
serverName = 'servername' #bare brugt til information i handshake, den er overvejende unoedvendig
serverPort = 12004
clientSocket = socket(AF_INET, SOCK_STREAM)
clientSocket.connect(('192.168.1.194', serverPort))
sentence = ('disse bogstaver bliver skrevet i all caps')
clientSocket.sendall(sentence.encode('utf-8')) #text skal encodes til unicode for at sendes
modifiedSentence = clientSocket.recv(1024)
print ("From Server:", modifiedSentence) #print ændret besked fra server
clientSocket.close()