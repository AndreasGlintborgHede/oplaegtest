#!/usr/bin/python           # This is server.py file


from socket import *  #imortering af socket modul
serverPort = 12004 #reserver port
serverSocket = socket(AF_INET, SOCK_STREAM) #socket familie tcp
serverSocket.bind(('192.168.1.194', 12004)) #Ip og port der hoster socket bindes
serverSocket.listen(1) #definer laengde af koeen
print ("server er klar til at modtage") 
while 1: 
    connectionSocket, addr = serverSocket.accept() #etabler forbindelse
    sentence = connectionSocket.recv(1024) #laes maks 1024 bytes data
    capitalizedSentence = sentence.upper() #saet besked fra client til uppercase
    connectionSocket. send(capitalizedSentence) #send til client
    connectionSocket.close()